<?php

use App\services\ServiceException;
use Phalcon\Mvc\Micro;

error_reporting(E_ALL);
ini_set("display_errors", 0);
ini_set("log_errors", 1);

//Define where do you want the log to go, syslog or a file of your liking with
//ini_set("error_log", "syslog");

try {

    // Composer autoloader
    require_once(__DIR__ . '/../vendor/autoload.php');

    // Loading Configs
    $config = require(__DIR__ . '/../app/config/config.php');

    // Autoloading classes
    require __DIR__ . '/../app/config/loader.php';

    // Initializing DI container
    /** @var \Phalcon\DI\FactoryDefault $di */
    require __DIR__ . '/../app/config/di.php';

    $app = new Micro();

    // Setting DI container
    $app->setDI($di);

    // Setting up routing
    require __DIR__ . '/../app/config/routes.php';


// Making the correct answer after executing
    $app->after(
        function () use ($app) {
// Getting the return value of method
            $return = $app->getReturnedValue();

            if (is_array($return)) {
// Transforming arrays to JSON
                $app->response->setContent(json_encode(['KEY_CODE'=>0, 'KEY_MESSAGE'=>'OK', 'DATA'=>$return]));
            } else {
// Unexpected response
                throw new Exception('Bad Response');
            }

        }
    );

    $app->handle();
} catch (ServiceException $e) {
    $app->response->setStatusCode( substr($e->getCode(),-3), $e->getMessage());
    $result = [
        "KEY_CODE" => $e->getCode(),
        "KEY_MESSAGE" => $e->getMessage(),
    ];

// Set error response
    $app->response->setJsonContent($result);

} catch (\Throwable $e) {
// Standard error format
    $result = [
        "KEY_CODE" => 500,
        "KEY_MESSAGE" => 'Some error occurred on the server.',
        "ExceptionTrace" => print_r($e->getTrace(), 1),
        "ExceptionText" => $e->getMessage()

    ];

// Sending error response
    $app->response->setStatusCode(500, 'Internal Server Error')
        ->setJsonContent($result);
}

// Sending response to the client
$app->response->send();