CREATE DATABASE  IF NOT EXISTS `phonebook` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `phonebook`;
-- MySQL dump 10.13  Distrib 8.0.13, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: phonebook
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `phonebook`
--

DROP TABLE IF EXISTS `phonebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `phonebook` (
  `phonebook_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `phoneNumber` varchar(15) NOT NULL,
  `countryCode` varchar(45) DEFAULT NULL,
  `timeZoneName` varchar(145) DEFAULT NULL,
  `insertedOn` datetime NOT NULL,
  `updatedOn` datetime NOT NULL,
  PRIMARY KEY (`phonebook_id`),
  UNIQUE KEY `phonebook_id_UNIQUE` (`phonebook_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-06  0:51:06


INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Petr', 'Petrov', '71234567891', 'RU', 'Pacific/Galapagos', '2019-09-06 05:30:36', '2019-09-06 05:30:36');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');
INSERT INTO phonebook.phonebook (phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn) VALUES (null, 'Ivan', 'Ivanov', '72345678901', 'RU', 'Pacific/Galapagos', '2019-09-06 15:31:46', '2019-09-06 15:31:49');