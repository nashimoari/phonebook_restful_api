<?php

use Phalcon\Mvc\Micro;
use Phalcon\Http\Request;

// Loading Configs
$config = require(__DIR__ . '/../../app/config/config_tests.php');

// Autoloading classes
require (__DIR__ . '/../../app/config/loader.php');

// Required for phalcon/incubator
include (__DIR__ . "/../../vendor/autoload.php");

// Initializing DI container
/** @var \Phalcon\DI\FactoryDefault $di */
require (__DIR__ . '/../../app/config/di.php');

//var_dump($di);

$app = new Micro();

// Setting DI container
$app->setDI($di);

// Setting up routing
require __DIR__ . '/../../app/config/routes.php';


// Making the correct answer after executing
$app->after(
    function () use ($app) {
// Getting the return value of method
        $return = $app->getReturnedValue();

        if (is_array($return)) {
// Transforming arrays to JSON
            $app->response->setContent(json_encode($return));
        } elseif (!strlen($return)) {
// Successful response without any content
            $app->response->setStatusCode('204', 'No Content');
        } else {
// Unexpected response
            throw new Exception('Bad Response');
        }
    }
);

// https://stackoverflow.com/questions/34981380/modelsmanager-not-set-in-phalcon-unittest
// Setup parent:
parent::setUp();
// --------------------------------------------------
// THIS IS WHAT SOLVED IT FOR ME, since Phalcon
// Set default DI
// uses the \Phalcon\Di::getDefault() for handling queries from a model:
\Phalcon\Di::setDefault($di);
