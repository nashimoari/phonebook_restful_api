<?php

namespace Tests\PhoneBookTest;

use Tests\UnitTestCase;
use Phalcon\Mvc\Micro;
use GuzzleHttp\Client;
use Phalcon\DI;

final class PhoneBookFunctionalTest extends UnitTestCase
{
    private static $DBInit;

    /**
     * @before
     */
    protected function firstSetUp()
    {
        if (self::$DBInit) {
            return;
        }

        /* your one time setUp here */

        self::$DBInit = true;
        $this->DBPrepare();
    }

    /**
     * Prepare database for tests
     */
    public function DBPrepare()
    {
        $client = new Client();
        $resp = $client->request('DELETE', 'http://localhost/api/phoneBooks/DBDrop', ['http_errors' => false]);

        $resp = $client->request('POST', 'http://localhost/api/phoneBooks/DBInit', ['http_errors' => false]);
    }

    public function testGetIncorrectMethod()
    {
        $client = new Client();
        $resp = $client->request('GET', 'http://localhost/api/phoneBooks/absent_method', ['http_errors' => false]);

        // check response
        $this->assertEquals(500, $resp->getStatusCode());
        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(11404, isset($requestResult['KEY_CODE']));
        $this->assertEquals('Some error occurred on the server.', $requestResult['KEY_MESSAGE']);
    }

    public function testGetAbsentOneRecord()
    {
        $client = new Client();
        $resp = $client->request('GET', 'http://localhost/api/phoneBooks/1000000', ['http_errors' => false]);

        // check response
        $this->assertEquals(404, $resp->getStatusCode());
        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(11404, isset($requestResult['KEY_CODE']));
        $this->assertEquals('Record is absent', $requestResult['KEY_MESSAGE']);
    }


    public function testAddRecordIncorrectPhoneNumber()
    {
        $record = ['firstName' => 'Petr', 'lastName' => 'Petrov', 'phoneNumber' => '712345a67891', 'countryCode' => 'RU', 'timeZoneName' => 'Pacific/Galapagos'];

        $client = new Client();

        $resp = $client->request('POST', 'http://localhost/api/phoneBooks', ['form_params' => $record, 'http_errors' => false]);

        // check response
        $this->assertEquals(400, $resp->getStatusCode());
    }

    public function testAddRecordIncorrectCountryCode()
    {
        $record = ['firstName' => 'Petr', 'lastName' => 'Petrov', 'phoneNumber' => '71234567891', 'countryCode' => 'unknown country code', 'timeZoneName' => 'Pacific/Galapagos'];

        $client = new Client();

        $resp = $client->request('POST', 'http://localhost/api/phoneBooks', ['form_params' => $record, 'http_errors' => false]);

        // check response
        $this->assertEquals(400, $resp->getStatusCode());

    }

    public function testAddRecordIncorrectTimeZone()
    {
        $record = ['firstName' => 'Petr', 'lastName' => 'Petrov', 'phoneNumber' => '71234567891', 'countryCode' => 'RU', 'timeZoneName' => 'unknown time zone'];

        $client = new Client();

        $resp = $client->request('POST', 'http://localhost/api/phoneBooks', ['form_params' => $record, 'http_errors' => false]);

        // check response
        $this->assertEquals(400, $resp->getStatusCode());
    }


    public function testAddRecordCorrect()
    {
        $record = ['firstName' => 'Petr', 'lastName' => 'Petrov', 'phoneNumber' => '71234567891', 'countryCode' => 'RU', 'timeZoneName' => 'Pacific/Galapagos'];
        $client = new Client();
        $resp = $client->request('POST', 'http://localhost/api/phoneBooks', ['form_params' => $record, 'http_errors' => false]);
        $this->assertEquals(200, $resp->getStatusCode());
        $data = json_decode($resp->getBody(true), true);
    }

    /**
     * @depends testAddRecordCorrect
     */
    public function testListAll()
    {
        $client = new Client();
        $resp = $client->request('GET', 'http://localhost/api/phoneBooks', ['http_errors' => false]);
        $this->assertEquals(200, $resp->getStatusCode());
        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(true, is_array($requestResult));

        $this->assertEquals(true, isset($requestResult['DATA'][0]['phonebook_id']));
        $this->assertEquals(true, isset($requestResult['DATA'][0]['firstName']));
        $this->assertEquals(true, isset($requestResult['DATA'][0]['lastName']));
        $this->assertEquals(true, isset($requestResult['DATA'][0]['phoneNumber']));
        $this->assertEquals(true, isset($requestResult['DATA'][0]['countryCode']));
        $this->assertEquals(true, isset($requestResult['DATA'][0]['timeZoneName']));
        $this->assertEquals(true, isset($requestResult['DATA'][0]['insertedOn']));
        $this->assertEquals(true, isset($requestResult['DATA'][0]['updatedOn']));
    }

    public function testListPaginationAbsentOffset()
    {
        $client = new Client();
        $resp = $client->request('GET', 'http://localhost/api/phoneBooks?limit=5', ['http_errors' => false]);
        $this->assertEquals(200, $resp->getStatusCode());

        $requestResult = json_decode($resp->getBody(true), true);
        $this->assertEquals(1, $requestResult['DATA'][0]['phonebook_id']);
        $this->assertEquals(5, count($requestResult['DATA']));

    }

    public function testListPaginationAbsentLimit()
    {
        $client = new Client();
        $resp = $client->request('GET', 'http://localhost/api/phoneBooks?offset=5', ['http_errors' => false]);
        $this->assertEquals(200, $resp->getStatusCode());
        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(6, $requestResult['DATA'][0]['phonebook_id']);
    }

    public function testListPaginationCorrect()
    {
        $client = new Client();
        $resp = $client->request('GET', 'http://localhost/api/phoneBooks?offset=5&limit=10', ['http_errors' => false]);
        $this->assertEquals(200, $resp->getStatusCode());
        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(true, is_array($requestResult));
        $this->assertEquals(10, count($requestResult['DATA']));
        $this->assertEquals(6, $requestResult['DATA'][0]['phonebook_id']);
    }

    /**
     * @depends testAddRecordCorrect
     */
    public function testGetOneRecord()
    {
        $client = new Client();

        $resp = $client->request('GET', 'http://localhost/api/phoneBooks/1', ['http_errors' => false]);
        // check response
        $this->assertEquals(200, $resp->getStatusCode());

        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(true, isset($requestResult['DATA']['phonebook_id']));
        $this->assertEquals(true, isset($requestResult['DATA']['firstName']));
        $this->assertEquals(true, isset($requestResult['DATA']['lastName']));
        $this->assertEquals(true, isset($requestResult['DATA']['phoneNumber']));
        $this->assertEquals(true, isset($requestResult['DATA']['countryCode']));
        $this->assertEquals(true, isset($requestResult['DATA']['timeZoneName']));
        $this->assertEquals(true, isset($requestResult['DATA']['insertedOn']));
        $this->assertEquals(true, isset($requestResult['DATA']['updatedOn']));
    }


    public function testUpdateAbsentRecord()
    {
        $record = ['firstName' => 'Ivan', 'lastName' => 'Ivanov', 'phoneNumber' => '79876543210', 'countryCode' => 'RU', 'timeZoneName' => 'Pacific/Galapagos'];

        $client = new Client();

        $resp = $client->request('PUT', 'http://localhost/api/phoneBooks/1000000', ['form_params' => $record, 'http_errors' => false]);
        // check response
        $this->assertEquals(404, $resp->getStatusCode());


        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(true, is_array($requestResult));
        $this->assertEquals('Phonebook record absent', $requestResult['KEY_MESSAGE']);
        $this->assertEquals(11404, $requestResult['KEY_CODE']);
    }

    public function testUpdateIncorrectTimeZone()
    {
        $record = ['firstName' => 'Ivan', 'lastName' => 'Ivanov', 'phoneNumber' => '79876543210', 'countryCode' => 'RU', 'timeZoneName' => 'Unknown time zone'];

        $client = new Client();

        $resp = $client->request('PUT', 'http://localhost/api/phoneBooks/1', ['form_params' => $record, 'http_errors' => false]);
        // check response
        $this->assertEquals(400, $resp->getStatusCode());


        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(true, is_array($requestResult));
        $this->assertEquals('incorrect timeZone value', $requestResult['KEY_MESSAGE']);
        $this->assertEquals(11400, $requestResult['KEY_CODE']);
    }

    public function testUpdateIncorrectCountry()
    {
        $record = ['firstName' => 'Ivan', 'lastName' => 'Ivanov', 'phoneNumber' => '79876543210', 'countryCode' => 'unknown country', 'timeZoneName' => 'Pacific/Galapagos'];

        $client = new Client();

        $resp = $client->request('PUT', 'http://localhost/api/phoneBooks/1', ['form_params' => $record, 'http_errors' => false]);
        // check response
        $this->assertEquals(400, $resp->getStatusCode());

        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(true, is_array($requestResult));
        $this->assertEquals('incorrect countryCode value', $requestResult['KEY_MESSAGE']);
        $this->assertEquals(11400, $requestResult['KEY_CODE']);
    }

    /**
     * @depends testAddRecordCorrect
     */
    public function testUpdatePresentRecord()
    {
        $record = ['firstName' => 'Ivan', 'lastName' => 'Ivanov', 'phoneNumber' => '79876543210', 'countryCode' => 'RU', 'timeZoneName' => 'Pacific/Galapagos'];

        $client = new Client();

        $resp = $client->request('PUT', 'http://localhost/api/phoneBooks/1', ['form_params' => $record, 'http_errors' => false]);
        // check response
        $this->assertEquals(200, $resp->getStatusCode());

        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(true, is_array($requestResult));
        $this->assertEquals('OK', $requestResult['KEY_MESSAGE']);
    }

    public function testDeleteAbsentRecord()
    {
        $client = new Client();

        $resp = $client->request('DELETE', 'http://localhost/api/phoneBooks/1000000', ['http_errors' => false]);
        // check response
        $this->assertEquals(404, $resp->getStatusCode());

        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(true, is_array($requestResult));
        $this->assertEquals(11404, $requestResult['KEY_CODE']);
    }

    /**
     * @depends testUpdatePresentRecord
     */
    public function testDeleteRecord()
    {
        $client = new Client();

        $resp = $client->request('DELETE', 'http://localhost/api/phoneBooks/1', ['http_errors' => false]);
        // check response
        $this->assertEquals(200, $resp->getStatusCode());

        $requestResult = json_decode($resp->getBody(true), true);

        $this->assertEquals(true, is_array($requestResult));
        $this->assertEquals(0, $requestResult['KEY_CODE']);

        // check that a record is deleted
        $client = new Client();
        $resp = $client->request('GET', 'http://localhost/api/phoneBooks/1', ['http_errors' => false]);

        // check response
        $this->assertEquals(404, $resp->getStatusCode());
    }
}