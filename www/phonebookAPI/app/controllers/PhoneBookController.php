<?php


namespace App\Controllers;

use App\Services\PhoneBookService;
use Phalcon\DI;

/**
 * Operations with phone book: CRUD
 */
class PhoneBookController extends AbstractController
{
    private $phoneBookService;

    public function __construct()
    {
        $this->phoneBookService = new PhoneBookService();
    }

    public function DBDrop()
    {
        return $this->phoneBookService->prepareDBDrop();
    }

    public function DBInit()
    {
        return $this->phoneBookService->prepareDBInit();
    }

    /**
     * Adding phone book record
     */
    public function addPhoneBookRecord()
    {
        $newPhoneBookRecord = [];

        $newPhoneBookRecord['firstName'] = $this->request->getPost('firstName');
        $newPhoneBookRecord['lastName'] = $this->request->getPost('lastName');
        $newPhoneBookRecord['phoneNumber'] = $this->request->getPost('phoneNumber');
        $newPhoneBookRecord['countryCode'] = $this->request->getPost('countryCode');
        $newPhoneBookRecord['timeZoneName'] = $this->request->getPost('timeZoneName');
        return $this->phoneBookService->addPhoneBookRecord($newPhoneBookRecord);
    }

    /**
     * Returns all phone book record list
     *
     * @return array
     */
    public function getPhoneBookRecordListAction()
    {
        $di = Di::getDefault();
        $defaultLimit = $di->get('config')['application']['defaultPaginationLimit'];

        $limit = $this->request->getQuery('limit', 'int', $defaultLimit);
        $offset = $this->request->getQuery('offset', 'int', 0);


        $pageParams = ['limit' => $limit, 'offset' => $offset];

        return $this->phoneBookService->getPhoneBookRecords($pageParams);
    }

    /**
     * Return phone book record by id
     *
     * @param int $recordId
     * @return array
     */
    public function getPhoneBookRecordAction(int $recordId)
    {
        return $this->phoneBookService->getPhoneBookRecordById($recordId);
    }

    /**
     * Updating existing user
     *
     * @param string $recordId
     */
    public function updatePhoneBookRecordAction($recordId)
    {
        $phoneBookRecord = [];

        $phoneBookRecord['recordId'] = $recordId;
        $phoneBookRecord['firstName'] = $this->request->getPut('firstName');
        $phoneBookRecord['lastName'] = $this->request->getPut('lastName');
        $phoneBookRecord['phoneNumber'] = $this->request->getPut('phoneNumber');
        $phoneBookRecord['countryCode'] = $this->request->getPut('countryCode');
        $phoneBookRecord['timeZoneName'] = $this->request->getPut('timeZoneName');
        return $this->phoneBookService->updatePhoneBookRecordAction($phoneBookRecord);

    }

    /**
     * Delete an existing phone book record
     *
     * @param string $recordId
     */
    public function deletePhoneBookRecordAction($recordId)
    {
        return $this->phoneBookService->deletePhoneBookRecordAction($recordId);
    }
}