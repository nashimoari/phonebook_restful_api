<?php

namespace App\Models;

class Phonebook extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $phonebook_id;

    /**
     *
     * @var string
     */
    protected $firstName;

    /**
     *
     * @var string
     */
    protected $lastName;

    /**
     *
     * @var string
     */
    protected $phoneNumber;

    /**
     *
     * @var string
     */
    protected $countryCode;

    /**
     *
     * @var string
     */
    protected $timeZoneName;

    /**
     *
     * @var string
     */
    protected $insertedOn;

    /**
     *
     * @var string
     */
    protected $updatedOn;

    /**
     * Method to set the value of field phonebook_id
     *
     * @param integer $phonebook_id
     * @return $this
     */
    public function setPhonebookId($phonebook_id)
    {
        $this->phonebook_id = $phonebook_id;

        return $this;
    }

    /**
     * Method to set the value of field firstName
     *
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Method to set the value of field lastName
     *
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Method to set the value of field phoneNumber
     *
     * @param string $phoneNumber
     * @return $this
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Method to set the value of field countryCode
     *
     * @param string $countryCode
     * @return $this
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * Method to set the value of field timeZoneName
     *
     * @param string $timeZoneName
     * @return $this
     */
    public function setTimeZoneName($timeZoneName)
    {
        $this->timeZoneName = $timeZoneName;

        return $this;
    }

    /**
     * Method to set the value of field insertedOn
     *
     * @param string $insertedOn
     * @return $this
     */
    public function setInsertedOn($insertedOn)
    {
        $this->insertedOn = $insertedOn;

        return $this;
    }

    /**
     * Method to set the value of field updatedOn
     *
     * @param string $updatedOn
     * @return $this
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Returns the value of field phonebook_id
     *
     * @return integer
     */
    public function getPhonebookId()
    {
        return $this->phonebook_id;
    }

    /**
     * Returns the value of field firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Returns the value of field lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Returns the value of field phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Returns the value of field countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Returns the value of field timeZoneName
     *
     * @return string
     */
    public function getTimeZoneName()
    {
        return $this->timeZoneName;
    }

    /**
     * Returns the value of field insertedOn
     *
     * @return string
     */
    public function getInsertedOn()
    {
        return $this->insertedOn;
    }

    /**
     * Returns the value of field updatedOn
     *
     * @return string
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("phonebook");
        $this->setSource("phonebook");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'phonebook';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Phonebook[]|Phonebook|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Phonebook|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
