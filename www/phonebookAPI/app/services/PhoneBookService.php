<?php

namespace App\Services;

use App\Models\Phonebook;
use Phalcon\DI;


/**
 * business logic for phone book
 *
 * Class PhoneBookService
 */
class PhoneBookService extends AbstractService
{
    /** Unable to create user */
    const ERROR_UNABLE_CREATE_RECORD = 11500;
    const ERROR_RECORD_ABSENT = 11404;
    const ERROR_UNABLE_DELETE_RECORD = 11500;
    const ERROR_VALIDATION_FAILED = 11400;
    const ERROR_THIRD_PARTY_ERROR = 11500;


    public function prepareDBDrop()
    {
        $data = ['Status' => 'OK'];
        $di = Di::getDefault();
        $client = $di->get('dbEmpty');

        $client->connect();

        $sql = 'DROP DATABASE phonebook';

        try {
            $result_set = $client->query($sql);
        } catch (\Throwable $e) {
            $data = ['Status' => $e->getCode() . ";" . $e->getMessage()];
        }
        return $data;
    }

    public function prepareDBInit()
    {
        $data = ['Status' => 'OK'];
        $di = Di::getDefault();
        $client = $di->get('dbEmpty');

        $client->connect();

        $DBDump = file_get_contents(__DIR__.'/../../phonebookDB.sql');
        $client->query($DBDump);
        return $data;
    }

    /**
     * Returns phone book records
     *
     * @return array
     */
    public function getPhoneBookRecords($pageParams)
    {
        try {

            if (!isset($pageParams['limit'])) {
                throw new ServiceException('Parameter limit is absent', self::ERROR_VALIDATION_FAILED);
            }

            if (!isset($pageParams['offset'])) {
                throw new ServiceException('Parameter offset is absent', self::ERROR_VALIDATION_FAILED);
            }

            $records = Phonebook::find([
                'conditions' => '',
                'bind' => [],
                'limit' => $pageParams['limit'],
                'offset' => $pageParams['offset'],
                'columns' => "phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn"
            ]);

            if (!$records) {
                return [];
            }

            return $records->toArray();
        } catch (\PDOException $e) {
            throw new ServiceException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getPhoneBookRecordById($recordId)
    {
        try {
            $record = Phonebook::findFirst(
                [
                    'conditions' => 'phonebook_id = :id:',
                    'bind' => [
                        'id' => $recordId
                    ],
                    'columns' => "phonebook_id, firstName, lastName, phoneNumber, countryCode, timeZoneName, insertedOn, updatedOn"
                ]
            );

            if (!$record) {
                throw new ServiceException('Record is absent', self::ERROR_RECORD_ABSENT);
            }

            return $record->toArray();
        } catch (\PDOException $e) {
            throw new ServiceException($e->getMessage(), $e->getCode(), $e);
        }

    }


    /**
     * Creating a new record of PhoneBook
     *
     * @param array $recordData
     */
    public function addPhoneBookRecord(array $recordData)
    {
        try {

            $this->validateData($recordData);

            $insertedOn = date('Y-m-d H:i:s');

            $phoneBook = new Phonebook();
            $result = $phoneBook->setFirstName($recordData['firstName'])
                ->setLastName($recordData['lastName'])
                ->setCountryCode($recordData['countryCode'])
                ->setPhoneNumber($recordData['phoneNumber'])
                ->setTimeZoneName($recordData['timeZoneName'])
                ->setInsertedOn($insertedOn)
                ->setUpdatedOn($insertedOn)
                ->create();

            if (!$result) {
                throw new ServiceException('Unable to create phonebook record', self::ERROR_UNABLE_CREATE_RECORD);
            }

            return ['insertId' => $phoneBook->getPhonebookId()];

        } catch (\PDOException $e) {
            if ($e->getCode() == 23505) {
                throw new ServiceException('phonebook record already exists', self::ERROR_ALREADY_EXISTS, $e);
            } else {
                throw new ServiceException($e->getMessage(), $e->getCode(), $e);
            }
        }
    }

    public function updatePhoneBookRecordAction(array $recordData)
    {

        try {
            $updatedOn = date('Y-m-d H:i:s');

            $phoneBook = Phonebook::findFirst(
                [
                    'conditions' => 'phonebook_id = :id:',
                    'bind' => [
                        'id' => $recordData['recordId']
                    ]
                ]
            );

            if (!$phoneBook) {
                throw new ServiceException('Phonebook record absent', self::ERROR_RECORD_ABSENT);
            }

            $this->validateData($recordData);

            //error_log(var_dump($phoneBook));

            $result = $phoneBook->setFirstName($recordData['firstName'])
                ->setLastName($recordData['lastName'])
                ->setCountryCode($recordData['countryCode'])
                ->setPhoneNumber($recordData['phoneNumber'])
                ->setTimeZoneName($recordData['timeZoneName'])
                ->setUpdatedOn($updatedOn)
                ->update();

            if (!$result) {
                throw new ServiceException('Unable to update phonebook record', self::ERROR_UNABLE_CREATE_RECORD);
            }

            return ['status' => 'OK'];

        } catch (\Throwable $e) {
            throw new ServiceException($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function deletePhoneBookRecordAction($recordId)
    {
        if (!ctype_digit($recordId) || ($recordId < 0)) {
            $errors['userId'] = 'Id must be a positive integer';
        }

        try {
            $record = Phonebook::findFirst(
                [
                    'conditions' => 'phonebook_id = :id:',
                    'bind' => [
                        'id' => $recordId
                    ]
                ]
            );

            if (!$record) {
                throw new ServiceException("Phonebook record not found", self::ERROR_RECORD_ABSENT);
            }

            $result = $record->delete();

            if (!$result) {
                throw new ServiceException('Unable to delete user', self::ERROR_UNABLE_DELETE_RECORD);
            }

            return ['KEY_CODE' => '0'];

        } catch (\PDOException $e) {
            throw new ServiceException($e->getMessage(), $e->getCode(), $e);
        }


    }


    private function validateData($recordData)
    {
        $countriesUrl = 'https://api.hostaway.com/countries';
        $timeZonesUrl = 'https://api.hostaway.com/timezones';


        if (preg_match("/^(\+\d{12}|\d{11}|\+\d{2}-\d{3}-\d{7})$/", $recordData['phoneNumber']) != 1) {
            throw new ServiceException('incorrect phone number', self::ERROR_VALIDATION_FAILED);
        }

        /** @var  $countriesRes array retrieve countries for validate */
        $countriesRes = $this->getValidationJson($countriesUrl);
        if ($countriesRes['status'] != 'success') {
            throw new ServiceException('can\'t get countries from ' . $countriesUrl, self::ERROR_THIRD_PARTY_ERROR);
        }

        /** @var  $countriesArr array countries codes */
        $countriesArr = $countriesRes['result'];

        if (!isset($countriesArr[$recordData['countryCode']])) {
            throw new ServiceException('incorrect countryCode value', self::ERROR_VALIDATION_FAILED);
        }


        $timeZonesRes = $this->getValidationJson($timeZonesUrl);
        if ($timeZonesRes['status'] != 'success') {
            throw new ServiceException('can\'t get timezones from ' . $timeZonesUrl, self::ERROR_THIRD_PARTY_ERROR);
        }
        $timeZonesArr = $timeZonesRes['result'];

        if (!isset($timeZonesArr[$recordData['timeZoneName']])) {
            throw new ServiceException('incorrect timeZone value', self::ERROR_VALIDATION_FAILED);
        }

    }

    private function getValidationJson(string $url): array
    {
        $di = Di::getDefault();

        // check present in cache
        $cache = $di->get('APICallCache');

        $cacheData = $cache->get($url);

        if ($cacheData) {
            return $cacheData;
        }


        $client = $di->get('GuzzleHttpClient');

        $resp = $client->request('GET', $url);
        // check response
        $respCode = $resp->getStatusCode();
        if ($respCode != 200) {
            throw new ServiceException('response code from ' . $url . ' : ' . $respCode, self::ERROR_THIRD_PARTY_ERROR);
        }

        $responseJson = json_decode($resp->getBody(), true);

        // save data to cache
        $cache->save($url, $responseJson);


        //echo $countriesData->getHeader('content-type')[0];
        //        'application/json'
        return $responseJson;
    }
}