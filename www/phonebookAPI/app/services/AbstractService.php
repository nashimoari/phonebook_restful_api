<?php

namespace App\Services;

use Phalcon\DI\Injectable;

/**
 * Class AbstractService
 *
 * @property \Phalcon\Db\Adapter\Pdo\Mysql $db
 * @property \Phalcon\Config                    $config
 */
abstract class AbstractService extends Injectable
{
    /**
     * Invalid parameters anywhere
     */
    const ERROR_INVALID_PARAMETERS = 10400;

    /**
     * Record already exists
     */
    const ERROR_ALREADY_EXISTS = 10400;
}