<?php

return new \Phalcon\Config(
    [
        'database' => [
            'adapter' => 'Mysql',
            'host' => 'mysql',
            'port' => 3306,
            'username' => 'root',
            'password' => 'PhoneBookAPI',
            'dbname' => 'phonebook',
        ],

        'application' => [
            'controllersDir' => "app/controllers/",
            'modelsDir' => "app/models/",
            'baseUri' => "/",
            'defaultPaginationLimit' => 100
        ],
    ]
);