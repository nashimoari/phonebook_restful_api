<?php

use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\DI\FactoryDefault;
use Phalcon\Http\Response;
use GuzzleHttp\Client;
use Phalcon\Cache\Backend\Memory;
use Phalcon\Cache\Frontend\Data as FrontData;

// Initializing a DI Container
$di = new FactoryDefault();

/**
 * Overriding Response-object to set the Content-type header globally
 */
$di->setShared(
    'response',
    function () {
        $response = new Response();
        $response->setContentType('application/json', 'utf-8');

        return $response;
    }
);

/** Common config */

$di->setShared('config', $config);

/** Database */
$di->set(
    "db",
    function () use ($config) {
        return new MySQL(
            [
                "host" => $config->database->host,
                "username" => $config->database->username,
                "password" => $config->database->password,
                "dbname" => $config->database->dbname,
            ]
        );
    }
);

/** Database without dbname for testing */
$di->set(
    "dbEmpty",
    function () use ($config) {
        return new MySQL(
            [
                "host" => $config->database->host,
                "username" => $config->database->username,
                "password" => $config->database->password,
            ]
        );
    }
);

$di->set(
    'modelsManager',
    function () {
        return new \Phalcon\Mvc\Model\Manager();
    }
);




$di->set('GuzzleHttpClient',
    function () {
        return new Client();
    }
);

$di->set('APICallCache', function () {$frontCache = new FrontData(); return new Memory($frontCache); });



