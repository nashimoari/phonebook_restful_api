<?php

use Phalcon\Mvc\Micro\Collection;

$usersCollection = new Collection();
$usersCollection->setHandler('\App\Controllers\PhoneBookController', true);
$usersCollection->setPrefix('/phoneBooks');
$usersCollection->post('/', 'addPhoneBookRecord');
$usersCollection->get('/', 'getPhoneBookRecordListAction');
$usersCollection->get('/{recordId:[1-9][0-9]*}', 'getPhoneBookRecordAction');
$usersCollection->put('/{recordId:[1-9][0-9]*}', 'updatePhoneBookRecordAction');
$usersCollection->delete('/{recordId:[1-9][0-9]*}', 'deletePhoneBookRecordAction');

$usersCollection->delete('/DBDrop', 'DBDrop');
$usersCollection->post('/DBInit', 'DBInit');

$app->mount($usersCollection);

// not found URLs
$app->notFound(
    function () use ($app) {
        $exception =
            new \App\Controllers\HttpExceptions\Http404Exception(
                'URI not found or error in request. ('.$app->request->getMethod() . ' ' . $app->request->getURI().')',
                \App\Controllers\AbstractController::ERROR_NOT_FOUND,
                new \Exception('URI not found: ' . $app->request->getMethod() . ' ' . $app->request->getURI())
            );
        throw $exception;
    }
);